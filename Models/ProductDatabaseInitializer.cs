﻿using System.Collections.Generic;
using System.Data.Entity;

namespace WingtipToys.Models
{
  public class ProductDatabaseInitializer : DropCreateDatabaseAlways<ProductContext>
  {
    protected override void Seed(ProductContext context)
    {
      GetCategories().ForEach(c => context.Categories.Add(c));
      GetProducts().ForEach(p => context.Products.Add(p));
    }

    private static List<Category> GetCategories()
    {
      var categories = new List<Category> {
                new Category
                {
                    CategoryID = 1,
                    CategoryName = "Handbags"
                },
                new Category
                {
                    CategoryID = 2,
                    CategoryName = "Laptop Bag"
                },
                new Category
                {
                    CategoryID = 3,
                    CategoryName = "Clutches"
                }
                /*,
                new Category
                {
                    CategoryID = 4,
                    CategoryName = "Boats"
                },
                new Category
                {
                    CategoryID = 5,
                    CategoryName = "Rockets"
                },*/
            };

      return categories;
    }

    private static List<Product> GetProducts()
    {
        /*  public int ProductID { get; set; }

    [Required, StringLength(100), Display(Name = "Code")]
    public string ProductCode { get; set; }

    [Required, StringLength(10000), Display(Name = "Product Description"), DataType(DataType.MultilineText)]
    public string Description { get; set; }

    public string ImagePath { get; set; }

    [Display(Name = "Price")]
    public double? UnitPrice { get; set; }

    public int? CategoryID { get; set; }

    public virtual Category Category { get; set; }
         */

        var products = new List<Product> 
        { 
            new Product 
            {
                ProductID = 1 , 
                ProductCode = "DU85", 
                Description= "Stone and orange leather handbag with Italian nickel buckles on adjustable straps.<br />The inner may be removed or exchanged for another colour making it a very versatile accessory. The outer piece has one cellphone pocket and the inner portion a leather trimmed zip pocket as well as two patch pockets.", 
                UnitPrice=4500.00, 
                ImagePath="DU85main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 2, 
                ProductCode = "BW78", 
                Description= "Light beige and black Italian leather double strap handbag with lamb nappa bow detail.  Nickel Italian hardware.  One internal leather trimmed zip pocket and two patch pockets.  Ykk Excella top zip closure", 
                UnitPrice=3500, 
                ImagePath="BW78main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 3, 
                ProductCode = "OC70", 
                Description= "Black full grain leather clutch bag with ostrich shin detail and magnetic clasp.", 
                UnitPrice=1750, 
                ImagePath="OC70main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 4,
                ProductCode = "GN69", 
                Description= "Go natural leather pleated soft bag with nickel Italian hardware.  Internal leather trimmed zip pocket and two patch pockets.", 
                UnitPrice=3850, 
                ImagePath="GN69main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 5, 
                ProductCode = "FOT63", 
                Description= "Dark khaki green oil pullup Italian full grain leather tote bag with nickel Italian hardware.  Internal leather trimmed zip pocket and two patch pockets.  Top zip closure.  Top section can be turned up to hold extra shopping/scarf or similar.", 
                UnitPrice=3350, 
                ImagePath="FOT63main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 6, 
                ProductCode = "SHO23", 
                Description= "Large blue Italian leather lined shopper with interchangeable scarf threaded through nickel Italian eyelets. Internal zipped leather pocket to hold valuables.", 
                UnitPrice=2750, 
                ImagePath="SHO23main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 7, 
                ProductCode = "BH68", 
                Description= "Tan coloured Italian leather shoulder bag with Nickel Italian hardware and top zip closure.  Internal leather trimmed zip pocket and two patch pockets.  Base is kidney shaped to wrap around the body.", 
                UnitPrice=3500, 
                ImagePath="BH68main.jpg",
                CategoryID=1,
                Quantity=4
            },
            new Product 
            {
                ProductID = 8, 
                ProductCode = "AS58", 
                Description= "Stone coloured assymetrical across body or shoulder bag with flap in full grain leather.  One external patch pocket and one internal leather trimmed zip pocket. Magnetic clasp under flap.  Double tassle accessory and contrasting piping.", 
                UnitPrice=2800, 
                ImagePath="AS58main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product
            {
                ProductID = 9, 
                ProductCode = "SB17", 
                Description= "Two tone Italian leather bowling style top zip bag with detachable long strap.  Braided detail on straps. Internal two leather trimmed patch pockets.  Nickel hardware from Italy.", 
                UnitPrice=2800, 
                ImagePath="SB17main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product {
                ProductID = 10, 
                ProductCode = "SS13", 
                Description= "Italian leather shoulder bag with flap as well as top zip closure with contrasting piping and detailing.  Internal leather trimmed zip pocket as well as two patch pockets.", 
                UnitPrice=2750, 
                ImagePath="SS13main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 11, 
                ProductCode = "SS15", 
                Description= "Italian leather shoulder bag with flap as well as top zip closure with contrasting piping and detailing.  Internal leather trimmed zip pocket as well as two patch pockets.", 
                UnitPrice=2750, 
                ImagePath="SS15main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 12, 
                ProductCode = "SS16", 
                Description= "Italian leather shoulder bag with flap as well as top zip closure with contrasting piping and detailing.  Internal leather trimmed zip pocket as well as two patch pockets.", 
                UnitPrice= 2750, 
                ImagePath="SS16main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 13, 
                ProductCode = "MS33", 
                Description= "Camel full grain Italian leather shopper type bag with contrasting external zip edging and three tone plaited handle. Two internal pockets.", 
                UnitPrice=2500, 
                ImagePath="MS33main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 14, 
                ProductCode = "FF55", 
                Description= "Black full grain leather and ostrich feather clutch or small shoulder bag with top zip closure and detachable strap.", 
                UnitPrice=2250, 
                ImagePath="FF55main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 15, 
                ProductCode = "SH5", 
                Description= "Two tone Italian leather hobo style bag with plaited leather handle and top zip closure. Internal leather trimmed zip pocket and two patch pockets", 
                UnitPrice=2725, 
                ImagePath="SH5main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 16, 
                ProductCode = "SH6", 
                Description= "Two tone Italian leather hobo style bag with plaited leather handle and top zip closure. Internal leather trimmed zip pocket and two patch pockets", 
                UnitPrice=2725, 
                ImagePath="SH6main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 17, 
                ProductCode = "SH7", 
                Description= "Two tone Italian leather hobo style bag with plaited leather handle and top zip closure. Internal leather trimmed zip pocket and two patch pockets", 
                UnitPrice=2725, 
                ImagePath="SH7main.jpg",
                CategoryID=1,
                Quantity=4
            },
            new Product 
            {
                ProductID = 18,
                ProductCode = "SC21", 
                Description= "Simple clutch leather bag with bow detail strap for holding. Magnetic clasp closure",
                UnitPrice=1250, 
                ImagePath="SC21main.jpg",
                CategoryID=1,
                Quantity=4
            },
            new Product 
            {
                ProductID = 19, 
                ProductCode = "SP35", 
                Description= "Slouchy soft beige Italian leather, long handled bag with black hand lacing and two tone tassles on flap. Can be worn across the body. Internal leather edged zip pocket plus two additional internal pockets.", 
                UnitPrice=3800, 
                ImagePath="SP35main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 20, 
                ProductCode = "IFF57", 
                Description= "Grey leather shoulder bag with ostrich feather detail. Magnetic clasp fastener, one internal leather trimmed zipped pocket and two patch pockets. Antique nickel hardware.", 
                UnitPrice=2800, 
                ImagePath="IFF57main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 21, 
                ProductCode = "ST1", 
                Description= "Italian leather tote bag with top zip closure, internal leather trimmed zip pocket as well as two patch pockets. Nickel hardware from Italy.", 
                UnitPrice=2800, 
                ImagePath="ST1main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 22, 
                ProductCode = "ST2", 
                Description= "Italian leather tote bag with top zip closure, internal leather trimmed zip pocket as well as two patch pockets. Nickel hardware from Italy.", 
                UnitPrice=2800,
                ImagePath="ST2main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 23, 
                ProductCode = "ST3", 
                Description= "Italian leather tote bag with top zip closure, internal leather trimmed zip pocket as well as two patch pockets. Nickel hardware from Italy.",
                UnitPrice=2800, 
                ImagePath="ST3main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 24, 
                ProductCode = "SSL10", 
                Description= "Across the body sling bag in Italian leather with external zip pocket and front patch/cellphone pocket. Top zip closure. Nickel hardware from Italy.", 
                UnitPrice=2300, 
                ImagePath="SSL10main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 25, 
                ProductCode = "SSL12", 
                Description= "Across the body sling bag in Italian leather with external zip pocket and front patch/cellphone pocket. Top zip closure. Nickel hardware from Italy.", 
                UnitPrice=2300, 
                ImagePath="SSL12main.jpg",
                CategoryID=1,
                Quantity=4
            }, 
            new Product 
            {
                ProductID = 26, 
                ProductCode = "NS52", 
                Description= "Shoulder bag in dull red/tan with two external zipped pockets, contrasting tassle detail, I internal zipped pocket and two internal patch pockets. Top zip closure and stud detail.", 
                UnitPrice=2550, 
                ImagePath="NS52main.jpg",
                CategoryID=1,
                Quantity=4
            }
        };
      return products;
    }
  }
}