﻿using System.ComponentModel.DataAnnotations;

namespace WingtipToys.Models
{
  public class Product
  {
    [ScaffoldColumn(false)]
    public int ProductID { get; set; }

    [Required, StringLength(100), Display(Name = "Code")]
    public string ProductCode { get; set; }

    [Required, StringLength(10000), Display(Name = "Product Description"), DataType(DataType.MultilineText)]
    public string Description { get; set; }

    public string ImagePath { get; set; }

    [Display(Name = "Price")]
    public double? UnitPrice { get; set; }

    public int Quantity { get; set; }

    public int? CategoryID { get; set; }

    public virtual Category Category { get; set; }
  }
}