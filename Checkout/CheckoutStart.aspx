﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CheckoutStart.aspx.cs" Inherits="WingtipToys.Checkout.CheckoutStart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%:Title %></h2>

    <div class="row">
        <div class="col-md-8">
            <section id="paymentDetailsForm">
                <div class="form-group">
                    <%-- <asp:ImageButton ID="CheckoutImageBtn" runat="server"
                        ImageUrl="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif"
                        Width="145" AlternateText="Checkout with PayPal"
                        OnClick="CheckoutBtn_Click"
                        BackColor="Transparent" BorderWidth="0" /> --%>
                </div>
                <div class="form-group">
                    <%-- <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Credit Card Number</asp:Label> --%>
                    <label for="creditCardNumber" class="col-md-2 control-label">Card Number</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="creditCardNumber" CssClass="form-control" ToolTip="Enter your credit card number" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="creditCardNumber"
                            CssClass="text-danger" ErrorMessage="A credit card number is required for payment." />
                    </div>
                </div>
                <div class="form-group">
                    <%-- <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Credit Card Number</asp:Label> --%>
                    <label for="creditCardAccountHolder" class="col-md-2 control-label">Card Holder</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="creditCardAccountHolder" CssClass="form-control" ToolTip="Enter the name of the card holder." />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="creditCardAccountHolder"
                            CssClass="text-danger" ErrorMessage="The card holder is required." />
                    </div>
                </div>
                <div class="form-group">
                    <label for="DropDownCardType" class="col-md-2 control-label">Card type</label>
                    <div class="col-md-10">
                        <asp:DropDownList ID="DropDownCardType" runat="server" ToolTip="Select the type of your credit card.">
                            <asp:ListItem>Select one...</asp:ListItem>
                            <asp:ListItem>Visa</asp:ListItem>
                            <asp:ListItem>Mastercard</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label for="DropDownExpirationMonth" class="col-md-2 control-label">Expiration Date</label>
                    <div class="col-md-10">
                        <asp:DropDownList ID="DropDownExpirationMonth" runat="server" ToolTip="Select the month your credit card expires.">
                            <asp:ListItem>Month</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem>09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DropDownExpirationYear" runat="server" ToolTip="Select the year that your credit card expires.">
                            <asp:ListItem>Year</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </section>
            <section id="deliveryDetailsForm">
                <div class="form-group">
                    <%-- <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Credit Card Number</asp:Label> --%>
                    <label for="streetAddress" class="col-md-2 control-label">Street address</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="streetAddress" CssClass="form-control" ToolTip="Enter your street address." />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="streetAddress"
                            CssClass="text-danger" ErrorMessage="A street address is required for delivery." />
                    </div>
                </div>

                <div class="form-group">
                    <%-- <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Credit Card Number</asp:Label> --%>
                    <label for="suburb" class="col-md-2 control-label">Suburb</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="suburb" CssClass="form-control" ToolTip="Enter your suburb" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="suburb"
                            CssClass="text-danger" ErrorMessage="A suburb is required for delivery." />
                    </div>
                </div>

                <div class="form-group">
                    <%-- <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Credit Card Number</asp:Label> --%>
                    <label for="cityTown" class="col-md-2 control-label">City/Town</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="cityTown" CssClass="form-control" ToolTip="Enter your city or town name" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="cityTown"
                            CssClass="text-danger" ErrorMessage="A city or town name is required for delivery." />
                    </div>
                </div>

                <div class="form-group">
                    <%-- <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Credit Card Number</asp:Label> --%>
                    <label for="postalCode" class="col-md-2 control-label">Postal Code</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="postalCode" CssClass="form-control" ToolTip="Enter your postal code." />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="postalCode"
                            CssClass="text-danger" ErrorMessage="A postal code is required for delivery." />
                    </div>
                </div>

                <div class="form-group">
                    <%-- <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Credit Card Number</asp:Label> --%>
                    <label for="country" class="col-md-2 control-label">Country</label>
                    <div class="col-md-10">
                        <asp:TextBox runat="server" ID="country" CssClass="form-control" ToolTip="Enter the country of delivery" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="country"
                            CssClass="text-danger" ErrorMessage="A country name is required for delivery." />
                    </div>
                </div>
            </section>
        </div>
    </div>




</asp:Content>
