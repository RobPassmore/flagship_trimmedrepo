﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WingtipToys.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %> Flagship</h2>
    
    <p>Sue Passmore has enjoyed a lifelong passion for creative activities including fashion design and sewing. She also studied interior design at Inscape Design College in Johannesburg.

In 2009 after enrolling in a formal training course in London with Master Craftsman, Anthony Vrahimis, Sue gained the skill required to cut patterns and construct handbags commercially, as well as finding a vocation combining all her talents and skills.

From her workshop Sue now focuses on creating distinct but timeless individually crafted handbags in line with the move away from mass production and brand fixation. She is personally responsible from conceptualisation to completion and is passionate about excellent workmanship and attention to detail.

Sue also understands the personal relationship a woman has with her handbag and realises the importance of this to potential customers.</p>
</asp:Content>
