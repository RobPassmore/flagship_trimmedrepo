﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WingtipToys.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class = "row">
        <div class="col-md-6">
            <section id="ContactForm">
                <div class="form-horizontal">
                    <h2>Contact Us</h2>
                    <hr />

                    <p>Please enter your contact details.</p>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="col-md-2 control-label">First Name</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" ToolTip="Enter your name" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                                CssClass="text-danger" ErrorMessage="First Name is required." />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="LastName" CssClass="col-md-2 control-label">Last Name</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="LastName" CssClass="form-control" ToolTip="Enter your surname" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                                CssClass="text-danger" ErrorMessage="Last Name is required." />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="Email" CssClass="form-control" ToolTip="Enter your email address" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                    CssClass="text-danger" ErrorMessage="Email address is required." />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" CssClass="col-md-2 control-label">Message</asp:Label>
                        <div class="col-md-10">
                            <textarea name="Notes" id="Notes" class="txt1h disableme" style="height:150px"></textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-offset-8 col-md-10">
                            <br />
                            <asp:Button runat="server" Text="Submit Enquiry" CssClass="btn btn-default" ToolTip="Submit Enquiry" />
                        </div>
                    </div>
                </div>
            </section>
        </div>
       
          <div class="col-md-6">

          <h3 style="padding-top:100px; text-align: right" > FLAGSHIP HANDBAGS</h3>
                <h5 style="text-align: right">Johannesburg, South Africa </h5>
                
               <h5 style="text-align: right">  Tel: +27 (0) 11 886 4201 </h5>
               <h5 style="text-align: right">  Fax: +27 (0) 11 781 5092 </h5>
               <h5 style="text-align: right">  Email: enquiries@flagship-handbags.co.za </h5>

        </div>



    </div>
</asp:Content>
