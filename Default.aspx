﻿<%@ Page Title="Welcome" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WingtipToys._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <asp:Image  ID="Image1" runat="server" ImageUrl="~/Images/RPP6895.jpg" BorderStyle="None" />
                </div>
                <div class="item">
                    <asp:Image  ID="Image2" runat="server" ImageUrl="~/Images/logo.png" BorderStyle="None" />
                </div>
            </div>

        </div>

       
            <br />  
  
        <p class="lead">The hallmarks of a Flagship handbag are simplicity, wearability, and discreet luxury.
            These independently handmade products will outlast any trend and truly earn their keep.</p>
            <p class="lead">Each Flagship handbag is individually crafted from superior quality Italian leather
            and hardware with an emphasis on excellent workmanship and attention to detail.
            You will have a personal interaction with the designer resulting in a well-informed,
            and satisfying purchasing experience.</p>
</asp:Content>
