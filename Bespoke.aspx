﻿<%@ Page Title="Bespoke" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Bespoke.aspx.cs" Inherits="WingtipToys.Bespoke" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <h2><%: Title %></h2>

    <div class="row">
       <div class="col-md-6">
           <section id="ProductDetails">

               <h4>Bespoke Enquiry</h4>
                <hr />
               
               <p style="padding-left:100px">Select bespoke options</p>

               <div class="form-group">
               <asp:Label runat="server" CssClass="col-md-2 control-label">Style Reference</asp:Label>
               <asp:DropDownList ID="StyleRef" runat="server" ItemType="WingtipToys.Models.Product"
                    Width="264px" Height="35px" SelectMethod="GetProducts" AppendDataBoundItems="true" 
                    DataTextField="ProductCode" DataValueField="ProductID"></asp:DropDownList>
                 </div>     
               <div class="form-group">
               <asp:Label runat="server" CssClass="col-md-2 control-label">Leather Colour</asp:Label>
               <asp:DropDownList ID="LeatherColour" runat="server" Width="264px" Height="35px">

                   <asp:ListItem>Black</asp:ListItem>
                     <asp:ListItem>White</asp:ListItem>
                     <asp:ListItem>Brown</asp:ListItem>
                     <asp:ListItem>Beige</asp:ListItem>
                   <asp:ListItem>Other</asp:ListItem>
                    




               </asp:DropDownList>
                 
           
               </div>   

               <div class="form-group">
               <asp:Label runat="server" CssClass="col-md-2 control-label">Lining</asp:Label>
               <asp:DropDownList ID="Lining" runat="server" Width="264px" Height="35px">

                   <asp:ListItem>Patterned Cotton</asp:ListItem>
                     <asp:ListItem>Printed Cotton</asp:ListItem>
                     <asp:ListItem>Cotton Twill</asp:ListItem>
                    
                    



               </asp:DropDownList>
                 </div>   

               
                   <div class="form-group">
               
               <asp:Label runat="server" CssClass="col-md-2 control-label"> Additional Notes</asp:Label>
                    <textarea name="Notes" id="Notes" class="txt1h disableme" ></textarea>
                 
                   </div>
           </section>

       </div>
        
        <div class="col-md-6">
            <section id="ContactForm">

                <div class="form-horizontal">
                    <h4>Contact Details</h4>
                    <hr />

                    <p style="padding-left:100px">Please enter your contact details.</p>

                    <div class="form-group">

                           <asp:Label runat="server" AssociatedControlID="FirstName" CssClass="col-md-2 control-label">First Name</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" ToolTip="Enter your name" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                                    CssClass="text-danger" ErrorMessage="First Name is required." />
                            </div>
                     </div>

                         <div class="form-group">

                        <asp:Label runat="server" AssociatedControlID="LastName" CssClass="col-md-2 control-label">Last Name</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="LastName" CssClass="form-control" ToolTip="Enter your surname" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                                    CssClass="text-danger" ErrorMessage="Last Name is required." />
                            </div>
                         </div>

                         <div class="form-group">
                         <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="Email" CssClass="form-control" ToolTip="Enter your email address" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                    CssClass="text-danger" ErrorMessage="Email address is required." />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <br />
                                <asp:Button runat="server" Text="Submit Enquiry" CssClass="btn btn-default" ToolTip="Submit Enquiry" />
      
                
                            </div>
                        </div>
                    
                   





                    </div>
               

           </section>

        </div>
    
    </div>
</asp:Content>


   